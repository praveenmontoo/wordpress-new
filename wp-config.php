<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_aws' );

/** Database username */
define( 'DB_USER', 'test' );

/** Database password */
define( 'DB_PASSWORD', 'Password' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'n=BRJT>DrY%^Ld^``avP1uw,7,4bySeYMlMw;3Mp*gLY(kESGY>[?=3phwkktKJi' );
define( 'SECURE_AUTH_KEY',  'ju=4LVgnNyhM<OM}I%rA7byj]{eN|LtwzO2;K6!t=[OLO8T56`gK$tsb]uKUPn2m' );
define( 'LOGGED_IN_KEY',    '/+}MSZJQWekqN)|LwPxJ9aG:Ar>k))KFCo_qgFbFS !IY3s+x.9vl2;|^E7mVy/B' );
define( 'NONCE_KEY',        '%C(,z<VeX+?5RM:F~cD 79>.=.Bd&{f$9ccd>CUZw1&JVzeqA:D[7Z-+dK},s#|(' );
define( 'AUTH_SALT',        '7[ c8#Q:A*@xym08opY9G.{~4FG-h1U$_B36fl>lbcQycu9`Vylc:x=Lon^L(G_S' );
define( 'SECURE_AUTH_SALT', '5Nqv/K5>E`DN]qA<pGQ{{I68q6f[*C+1w5K1~cP^~g2~9Oy62[$&)[eTizj nZP&' );
define( 'LOGGED_IN_SALT',   '|p%Oz7rRO//tg%3e3w/W?MextE!$S(Y2cfU<xqzGd2+|zqjN?VT:&zF*2RusQ)<[' );
define( 'NONCE_SALT',       '~(4Ew$J4b}wv0<VWeLU>:UX*lgdQk37vSp#r~vADGA/j+qH4P=wpB^ a[]0u$3f4' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
